package com.example.androidpresensesystem;

import android.view.View;

public interface ItemClickListenener {
    void onClick(View view, int position);
}
